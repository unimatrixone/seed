#!/bin/sh
set -e
SEED_URL="https://gitlab.com/unimatrixone/seed/-/raw/master"

curl -L --fail --silent -o Makefile ${SEED_URL}/Makefile
curl -L --fail --silent -o config.mk ${SEED_URL}/config.mk