include $SEMVER_FILE
include $PYPI_METADATA_FILE
include requirements.txt
include README.md
include VERSION
recursive-include $PYTHON_PKG_NAME VERSION
recursive-include $PYTHON_PKG_NAME **.html
recursive-include $PYTHON_PKG_NAME **.j2
recursive-include $PYTHON_PKG_NAME **.json
recursive-include $PYTHON_PKG_NAME **.yaml
recursive-include $PYTHON_PKG_NAME **.xml
