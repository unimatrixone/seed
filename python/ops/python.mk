PYTHON_PKG_NAME ?= $(error Define PYTHON_PKG_NAME in config.mk)
ifeq ($(PROJECT_SCOPE), namespaced)
PYTHON_SUBPKG_NAME ?= $(error Define PYTHON_SUBPKG_NAME in config.mk)
endif
PYTHON_SUBPKG_DIR ?= $(PYTHON_PKG_NAME)/ext
PYTHON_TEST_PACKAGES += bandit safety yamllint pylint twine sphinx

# Command-definitions
cmd.runtests = $(PYTHON) -m pytest -v
cmd.runtests += --cov-report term
cmd.runtests += --cov=$(PYTHON_PKG_NAME)
cmd.runtests += --cov-append
ifeq ($(PROJECT_SCOPE), namespaced)
cmd.runtests += $(PYTHON_SUBPKG_DIR)/$(PYTHON_SUBPKG_NAME)
else
cmd.runtests += $(PYTHON_PKG_NAME)
endif
