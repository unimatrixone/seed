# pylint: skip-file
import unittest

from .. import boot


class BootTestCase(unittest.TestCase):
    """Test if the boot function operates properly."""

    def test_invoke_boot_exits_succesfully(self):
        boot.boot()
