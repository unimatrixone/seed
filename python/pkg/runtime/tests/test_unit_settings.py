# pylint: skip-file
import importlib
import os
import unittest


class SettingsImportTestCase(unittest.TestCase):
    """Test if the application settings as specified by the environment
    variable ``UNIMATRIX_SETTINGS_MODULE`` import properly.
    """

    def setUp(self):
        if not os.getenv('UNIMATRIX_SETTINGS_MODULE'):
            self.fail('UNIMATRIX_SETTINGS_MODULE is not defined.')

    def test_import_settings(self):
        importlib.import_module('unimatrix.conf')
