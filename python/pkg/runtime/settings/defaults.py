"""The default application configuration. See
:func:`unimatrix.runtime.settings.for_environment()`
for more information.
"""