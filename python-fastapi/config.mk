# The name of this Python package, or the parent package if this is a
# namespaced package.
PYTHON_PKG_NAME=$(error Set PYTHON_PKG_NAME in config.mk)

# For FastAPI, only projects of kind 'application' are supported.
PROJECT_KIND=application

# Namespaced packages are not supported for FastAPI, so leave this to
# 'parent'.
PROJECT_SCOPE=parent

# The framework used to build this application or library. Supported Python
# frameworks are: fastapi.
LANGUAGE_FRAMEWORK=fastapi

# The Python version to use.
PYTHON_VERSION = 3.9

# Tables to truncate when invoking `make dbtruncate`, separated by a space.
#RDBMS_TRUNCATE=

# Components to configure.
mk.configure += python python-fastapi docker rdbms python-celery python-gitlab
mk.configure += python-sqlalchemy

# Configure the required coverage for testing stages.
test.coverage.unit := 13
test.coverage.integration := 82
test.coverage.system := 100
test.coverage := 100

# Configures the default OS platform. When overriding these variables,
# make sure to use the ?= operator as this value may also be provided as
# an environment variable during a CI run.
#OS_RELEASE_ID ?= alpine
#OS_RELEASE_VERSION ?= 3.12

# User-defined variables
