# pylint: skip-file
import unittest

from fastapi.testclient import TestClient
from unimatrix.conf import settings

from ..asgi import get_asgi_application


class ApplicationMetadataEndpointsTestCase(unittest.TestCase):

    def setUp(self):
        self.client = TestClient(get_asgi_application())

    def test_openapi_url_is_served(self):
        response = self.client.get(settings.OPENAPI_URL)
        self.assertEqual(response.status_code, 200)

    def test_docs_url_is_served(self):
        response = self.client.get(settings.DOCS_URL)
        self.assertEqual(response.status_code, 200)
