# pylint: skip-file
import asyncio
import unittest

from ..asgi import startup
from ..asgi import shutdown


class ApplicationEventsTestCase(unittest.TestCase):

    def test_startup(self):
        asyncio.run(startup())

    def test_shutdown(self):
        asyncio.run(shutdown())
