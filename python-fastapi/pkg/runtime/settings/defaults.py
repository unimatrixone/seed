"""Defaults settings for FastAPI projects."""

# Unimatrix-compliant Python packages.
INSTALLED_APPS = []


#: The OpenAPI URL exposed by the application.
OPENAPI_URL = "/openapi.json"

#: The URL at which the API browser is exposed.
DOCS_URL = "/ui"
