
DOCKER_REGISTRY=$(error Set DOCKER_REGISTRY in config.mk)

DOCKER_REPOSITORY=$(error Set DOCKER_REPOSITORY in config.mk)

DOCKER_TAG=1.20.2

DOCKER_BASE_IMAGE=docker.io/fluxcd/flux

LETSENCRYPT_EMAIL=$(error Set LETSENCRYPT_EMAIL in config.mk)

K8S_CLUSTER_NAME=$(error Set K8S_CLUSTER_NAME in config.mk)

mk.configure += k8s-kustomize