# https://www.gnupg.org/documentation/manuals/gnupg-devel/Unattended-GPG-key-generation.html
%no-protection
Key-Type: default
Key-Usage: encrypt,sign,auth
Key-Length: 4096
Name-Real: $K8S_CLUSTER_NAME
Name-Email: $K8S_CLUSTER_EMAIL
Expire-Date: 0