# pylint: disable=W0107
"""Declares :class:`ApplicationConfig`."""
from django.apps import AppConfig


class ApplicationConfig(AppConfig):
    """Configures the :mod:`$PYTHON_QUALNAME` package."""
    name = '$PYTHON_QUALNAME'
    label = '$DJANGO_APP_NAME'

    def ready(self):
        """Invoked when the Django app registry has loaded all
        apps.
        """
        pass