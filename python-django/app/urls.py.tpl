# pylint: disable=unused-import
"""Declares Django URL patterns for the :mod:`$PYTHON_QUALNAME` app."""
from django.conf import settings
from django.urls import include
from django.urls import path


app_name = '$DJANGO_APP_NAME'

urlpatterns = []