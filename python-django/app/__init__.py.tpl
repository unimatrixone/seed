# pylint: skip-file
"""Top-level module for the :mod:`$PYTHON_QUALNAME` module."""


default_app_config = '$PYTHON_QUALNAME.apps.ApplicationConfig'