#!/bin/sh
set -e
CURL="curl --fail --silent -L"
MAKE="make"
MODULE="python-django"
SEED_URL="https://gitlab.com/unimatrixone/seed/-/raw/master"
SEED_CONFIG="${SEED_URL}/${MODULE}/config-project.mk"


git init
touch Makefile\
  && git add Makefile\
  && git commit -m "Initial commit"\
  && git branch -M master mainline
${CURL} ${SEED_URL}/Makefile -o Makefile
${CURL} ${SEED_CONFIG} -o config.mk
if [ ! -z ${PROJECT_NAME+x} ]; then
  sed -i '' "s|PYTHON_PKG_NAME=.*|PYTHON_PKG_NAME=${PROJECT_NAME}|g" config.mk
fi
< /dev/tty vi -c "set ts=2 sw=2 noexpandtab syntax=make" config.mk
mkdir -p ./ops && make configure
