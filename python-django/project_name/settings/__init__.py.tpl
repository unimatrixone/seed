import importlib
import inspect
import os
import sys

from django.core.exceptions import ImproperlyConfigured

APP_ROLE = os.getenv('APP_ROLE')
DEPLOYMENT_ENV = os.getenv('DEPLOYMENT_ENV')
if DEPLOYMENT_ENV is None:
    raise ImproperlyConfigured(
        "DEPLOYMENT_ENV environment variable must not be empty.")

DEFAULTS_MODULE = '$PYTHON_PKG_NAME.runtime.settings.defaults'
SETTINGS_MODULE = '$PYTHON_PKG_NAME.runtime.settings.%s' % DEPLOYMENT_ENV
if APP_ROLE:
    DEFAULTS_MODULE = '$PYTHON_PKG_NAME.runtime.settings.%s.defaults' % APP_ROLE
    SETTINGS_MODULE = '$PYTHON_PKG_NAME.runtime.settings.%s.%s'\
        % (APP_ROLE, DEPLOYMENT_ENV)

try:
    base_settings = importlib.import_module(SETTINGS_MODULE)
except ImportError:
    base_settings = importlib.import_module(DEFAULTS_MODULE)
self = sys.modules[__name__]
for attname, value in inspect.getmembers(base_settings):
    if not str.isupper(attname):
        continue
    setattr(self, attname, value)
