# The name of this Python package, or the parent package if this is a
# namespaced package.
PYTHON_PKG_NAME=$(error Set PYTHON_PKG_NAME in config.mk)

# The subpackage name in a packaging namespace.
#PYTHON_SUBPKG_NAME = $(error Set PYTHON_SUBPKG_NAME in config.mk)

# Choose from 'application' or 'package'.
PROJECT_KIND=application

# Choose from 'parent' or 'namespaced'. If you are not sure, choose 'parent'.
# If PROJECT_SCOPE=namespaced, then PYTHON_SUBPKG_NAME must also be set.
PROJECT_SCOPE=parent

# The framework used to build this application or library. Supported Python
# frameworks are: django.
LANGUAGE_FRAMEWORK=django

# The Python version to use.
PYTHON_VERSION = 3.9

# Tables to truncate when invoking `make dbtruncate`, separated by a space.
#RDBMS_TRUNCATE=

# Components to configure.
mk.configure += python python-django docker rdbms
mk.configure +=
