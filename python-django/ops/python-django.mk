DB_ENGINE ?= sqlite
DB_NAME ?= db.sqlite3
DJANGO_SETTINGS_MODULE=unimatrix.ext.django.settings.runtime
DJANGO_SEED_URL=$(SEED_URL)/python-django
HTTP_LOCAL_HOST ?= 127.0.0.1
HTTP_LOCAL_PORT ?= 8000
HTTP_WSGI_MODULE ?= $(PYTHON_PKG_NAME).runtime.wsgi
RDBMS_MIGRATE=python manage.py migrate

# Since the Makefiles are mostly used during development and build, we
# can have some looser standards for a good DX.
CSRF_COOKIE_INSECURE ?= 1
SESSION_COOKIE_INSECURE ?= 1

# Command-definitions
cmd.migrate = $(PYTHON) manage.py migrate
cmd.migrations = $(PYTHON) manage.py makemigrations $(MODULE_NAME)
cmd.runhttp = $(PYTHON) manage.py runserver $(HTTP_LOCAL_HOST):$(HTTP_LOCAL_PORT)


ifeq ($(PROJECT_KIND), application)
UNIMATRIX_SETTINGS_MODULE = $(PYTHON_PKG_NAME).runtime.settings


manage.py: $(PYTHON_PKG_NAME)/runtime/settings $(PYTHON_PKG_NAME)/runtime/urls
	@$(cmd.curl) $(DJANGO_SEED_URL)/manage.py.tpl\
		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_PKG_NAME)/g'\
		> manage.py


$(PYTHON_PKG_NAME)/runtime:
	@mkdir -p $(PYTHON_PKG_NAME)/runtime


# The settings module may either be a Python module or a Python package. We
# assume by default a Package.
$(PYTHON_PKG_NAME)/runtime/settings: $(PYTHON_PKG_NAME)/runtime
	@mkdir -p $(PYTHON_PKG_NAME)/runtime/settings
	@$(MAKE) $(PYTHON_PKG_NAME)/runtime/settings/__init__.py
	@$(MAKE) $(PYTHON_PKG_NAME)/runtime/settings/base.py


bootstrap: manage.py
endif
