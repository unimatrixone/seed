#!/bin/bash
set -e
export APP_SECDIR=${APP_SECDIR-"/var/run/secrets/unimatrixone.io"}
CMD=$1
LOGLEVEL=${LOGLEVEL-"ERROR"}
HTTP_WORKERS=${HTTP_WORKERS-"4"}
HTTP_WORKER_TIMEOUT=${HTTP_WORKER_TIMEOUT-"600"}
HTTP_LOGLEVEL=${HTTP_LOGLEVEL-"${LOGLEVEL}"}
HTTP_PORT=${HTTP_PORT-"8000"}
HTTPS_PORT=${HTTPS_PORT-"8443"}
OCTET_BACKEND=${OCTET_BACKEND-"local"}
OCTET_PREFIX=${OCTET_PREFIX-"var/blob"}
PYTHON=${PYTHON-"python"}
if [ -z ${SECRET_KEY+x} ]; then
  export SECRET_KEY=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)
  echo "WARNING: No SECRET_KEY defined, using ephemeral" 1>&2
fi

if [ -f "COMMITHASH" ]; then
  export VCS_COMMIT_HASH=$(cat COMMITHASH)
fi

GUNICORN="${PYTHON} -m gunicorn.app.wsgiapp"
if [ ! -z ${HTTP_ASGI_MODULE} ]; then
  GUNICORN="${GUNICORN} ${HTTP_ASGI_MODULE}"
  GUNICORN_WORKER_CLASS=${GUNICORN_ASGI_WORKER_CLASS-"uvicorn.workers.UvicornWorker"}
  GUNICORN_ARGS="${GUNICORN_ARGS} -k ${GUNICORN_WORKER_CLASS}"
else
  GUNICORN="${GUNICORN} ${HTTP_WSGI_MODULE-unimatrix.ext.django.wsgi}"
  GUNICORN_ARGS="${GUNICORN_ARGS} -k sync"
fi

GUNICORN_ARGS="${GUNICORN_ARGS} --worker-tmp-dir /tmp --log-level ${HTTP_LOGLEVEL} -w ${HTTP_WORKERS}"
GUNICORN_ARGS="${GUNICORN_ARGS} --timeout ${HTTP_WORKER_TIMEOUT}"
if [ -f "$APP_PKIDIR/server/tls.key" ] && [ -f "$APP_PKIDIR/server/tls.crt" ]; then
  GUNICORN_ARGS="${GUNICORN_ARGS} --keyfile ${APP_PKIDIR}/server/tls.key"
  GUNICORN_ARGS="${GUNICORN_ARGS} --certfile ${APP_PKIDIR}/server/tls.crt"
  GUNICORN_ARGS="${GUNICORN_ARGS} -b 0.0.0.0:${HTTPS_PORT}"
else
  GUNICORN_ARGS="${GUNICORN_ARGS} -b 0.0.0.0:${HTTP_PORT}"
fi

if [ "$DEBUG" == "1" ]; then
  GUNICORN_ARGS="${GUNICORN_ARGS} --reload"
fi

if [ ! -z ${HTTP_MOUNT_PATH+x} ]; then
  export SCRIPT_NAME=$HTTP_MOUNT_PATH
fi

# Configure worker
CELERY="celery"
CELERY_MODULE=${CELERY_MODULE-"unimatrix.ext.worker"}
CELERY_LOGLEVEL=${CELERY_LOGLEVEL-"${LOGLEVEL}"}
CELERY_WORKER="${CELERY} -A ${CELERY_MODULE} worker --loglevel ${CELERY_LOGLEVEL}"
#CELERY_ARGS="--uid 1000 --gid 1000"
CELERY_ARGS="${CELERY_ARGS} --max-tasks-per-child 10"
if [ ! -z ${WORKER_CONCURRENCY} ]; then
  CELERY_ARGS="${CELERY_ARGS} -c ${WORKER_CONCURRENCY}"
fi
if [ -z ${CELERY_BROKER_URL+x} ]; then
  CELERY_BROKER_URL="sqla+postgresql://${DB_USERNAME}:${DB_PASSWORD}"
  CELERY_BROKER_URL="${CELERY_BROKER_URL}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
  export CELERY_BROKER_URL
fi

if [ -z ${DB_ENGINE+x} ] && ! ls "${APP_SECDIR}/rdbms/connections/*" &>/dev/null;
then
  echo "WARNING: No database engine specified through environment or configuration." 1>&2
  echo "WARNING: Defaulting to sqlite, /tmp/db.sqlite3" 1>&2
  export DB_ENGINE="sqlite"
  export DB_NAME="/tmp/db.sqlite3"
  export DB_URI="sqlite:///${DB_NAME}"
fi
if [ "$RDBMS_MIGRATE_ON_BOOT" == "1" ]; then
  $CMD_AWAITSERVICES
  eval $RDBMS_MIGRATE
fi

case $CMD in
  runworker)
    shift
    $CELERY_WORKER $CELERY_ARGS $@
  ;;
  runstatic)
    shift
    nginx $@
  ;;
  runhttp)
    shift
    $CMD_AWAITSERVICES
    $GUNICORN $GUNICORN_ARGS $@
  ;;
  runcmd)
    shift
    ${RUN_CMD} $@
  ;;
  *)
    $@
  ;;
esac
