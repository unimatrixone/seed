# Project Name

Add a short description of the project and its purpose.

This document describes how to set up, use and troubleshoot the local
development environment for the software project. For further details and
domain-specific documentaton, refer to the
[*Technical Documentation*](./docs/source/index.rst).

## TL;DR

- Run `make env` to set up the local development environment.
- Run all application components and infrastructure services in a terminal with
  the `make run` command.
- Run `make bash` to start a terminal configured with a proper development
  environment.
- If new requirements are added to the project (see `git log requirements.txt`),
  run `make depsrebuild`.
- Use `make resetinfra` to clear all persistent state maintained by the local
  development infrastructure (such as a database or cache server).
- Export the environment variables to a file with `make .env`. Use this command
  when integrating with third-party IDEs such as Eclipse or VSCode. Do not check
  in this file to the version control system - it is local to your environment
  only.
- Build the technical documentation with `make documentation`.
- Additional targets may be specified in [`config.mk`](./config.mk).

## Table of Contents

1. [Setting up local development](#setting-up-local-development)
   1. [Before you begin](#before-you-begin)
2. [Getting started](#getting-started)


## Setting up local development

The steps described in the sections below describe the procedure to prepare
your system to run the development environment and tools.

_It is assumed that the following POSIX utilities are present: awk mkdir rm
sed sha1sum sha256sum_

### Before you begin

#### Mac OS

- Ensure that Homebrew (the `brew` command) is installed by running
  `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null`.

#### General

1. Install GNU Make.
   - On Mac OS, GNU Make is included in the XCode Command Line Tools, which may
     be installed by running `xcode-select --install` in a terminal.
   - Linux users can install `make` from their operating system package manager.
2. Install cURL.
   - cURL is installed on Mac OS with `brew install curl`.
   - Linux users can install `curl` from their operating system package manager.
3. Install the latest version of Docker for your operating system.
   - [Docker for Desktop (Mac OS)](https://docs.docker.com/docker-for-mac/install/)
   - [Docker for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
4. On Linux platforms, Docker Compose must be installed manually.
   - [Install Docker Compose](https://docs.docker.com/compose/install/)
5. Install GNU Screen.
   - Linux users can install `screen` from their operating system package
     manager.
   - Mac OS users can install `screen` using Brew e.g. `brew install screen`.
6. Install the `jq` command-line utility.
   - [Installation instructions](https://stedolan.github.io/jq/download/)


## Getting started

Spawn the local development environment by issueing `make run`. This command
starts a GNU Screen session with the following windows:
1. A terminal that watches for file changes and runs the unit tests
   (`make test-unit`) continuously.
2. The logging output of the infrastructure services.
3. An HTTP server, if implemented.
4. An asynchronous worker daemon, if implemented.
5. An Enterprise Event Listener (EEL), if implemented.
6. A Command Handler, if implemented.

Additional windows may be started depending on your configuration.

- To switch between windows, press Ctrl+A followed by one of the numbers shown
  from the left bottom of your screen.
- To close a window, press Ctrl+C.
- In GNU Screen, you can not use your mouse wheel to scroll up through the
  terminal outputs. Press Ctrl+A followed Escape to scroll using your up and
  down keys.


## Troubleshooting

### The infrastructure services bind to ports that are in use

If you have services such as PostgreSQL or RabbitMQ running on your system
outside the scope of this project, a port binding clash might occur. To resolve
this, create a file named `local.mk` in the repository root and specify
an available port for the specific service. The environment variables used
to specify ports, are found in the projects' `docker-compose.yml`. If this
file does not exist yet, run `make docker-compose.yml`.


## License

All rights reserved.


## Author information

This software package was created by **Cochise Ruhulessin** for the
[Unimatrix One](https://cloud.unimatrixone.io) project.

- [Send me an email](mailto:cochise.ruhulessin@unimatrixone.io)
- [GitLab](https://gitlab.com/unimatrixone)
- [GitHub](https://github.com/cochiseruhulessin)
- [LinkedIn](https://www.linkedin.com/in/cochise-ruhulessin-0b48358a/)
- [Twitter](https://twitter.com/magicalcochise)
