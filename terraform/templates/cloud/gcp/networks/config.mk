GCP_PROJECT_NAME=$(error Set GCP_PROJECT_NAME in config.mk)

TF_VAR_project = $(GCP_PROJECT_NAME)

mk.configure += terraform python-minimal gitlab terraform-gitlab
mk.configure += terraform-gcp-networks.mk