# Google Cloud Platform Networks

This Terraform plan deploys Virtual Private Cloud (VPC) networks on
Google Cloud Platform (GCP).

## Before you begin

1. Create a Google Cloud project. For reference in this README, we will call this
   project `myproject`.
2. Ensure that you are authenticated by running `gcloud auth login`.
3. Install Python 3, cURL and GNU Make.

## How do I...

### Create a new VPC network

1. Run `make vpc-<your vpc name>`. The name of the VPC must consist of alphanumeric
   characters and dashes only, and must not begin or end with a dash.
2. Open `modules/networks/<your vpc name>/main.tf` to configure the VPC.