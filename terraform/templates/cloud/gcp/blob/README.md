# Google Cloud Storage (GCS) Bucket Configuration

This Terraform plan configures Google Cloud Storage (GCS). The buckets
are maintained in a single Google Cloud Platform (GCP) project, which
manages the complete lifecycle, access control and retention policies
for all resources in all environments.

## Core Concepts

### Application Storage Container (ASC)

An Application Storage Container (ASC) is a group of storage buckets that
provide internal storage for an application. By convention, ASCs are
numbered and prefixed with an environment indicator that consist of
`osc<environment abbreviation>`. The environments are specified in
`main.tf`. *Note that removing an environment from the configuration
will destroy all buckets in that environment.*

### Generic Storage Container (GSC)

A Generic Storage Container (GCS) is simply a storage bucket that has
no further opinions on how it should be configured.

## Configuration

The variables specified below must be configured in the `config.mk`
GNU Make include.

1. The `GCP_PROJECT_NAME` in `config.mk` specifies the GCP project to
   create the GCS buckets in.
2. The `BLOB_STORAGE_BASE_DOMAIN` configures the base domain that is
   appended to all bucket names. Since GCS requires the bucket names
   to be globally unique, this provides a means to ensure that all
   buckets can be created succesfully.
3. `DEFAULT_STORAGE_LOCATION` may optionally be specified to prepopulate
   the location for newly created buckets.
4. `TERRAFORM_SERVICE_ACCOUNT` - the service account used to execute these
   plans.

## How do I...

### Create a new Application Storage Container

1. Run `make asc-<my.bucket.name>`.
2. Open `modules/asc/<my.bucket.name>/main.tf` to further configure the bucket.

### Create a Generic Storage Container

1. Run `make gsc-<my.bucket.name>`.
2. Open `modules/gsc/<my.bucket.name>/main.tf` to further configure the bucket.
