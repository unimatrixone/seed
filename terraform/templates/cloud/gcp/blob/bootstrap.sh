#!/bin/sh
set -e
CURL="curl --fail --silent -L"
MAKE="make"
SEED_URL="https://gitlab.com/unimatrixone/seed/-/raw/master"
MODULE="terraform/templates/cloud/gcp/blob"

git init
${CURL} ${SEED_URL}/terraform/Makefile -o Makefile
${CURL} ${SEED_URL}/${MODULE}/config.mk -o config.mk
< /dev/tty vi -c "set ts=2 sw=2 noexpandtab syntax=make" config.mk
mkdir -p ./ops && make configure