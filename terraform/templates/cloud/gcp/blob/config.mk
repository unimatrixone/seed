GCP_PROJECT_NAME = $(error Set GCP_PROJECT_NAME in config.mk)

BLOB_STORAGE_BASE_DOMAIN = $(error Set BLOB_STORAGE_BASE_DOMAIN in config.mk)

DEFAULT_STORAGE_LOCATION = $(error Set DEFAULT_STORAGE_LOCATION in config.mk)

TERRAFORM_SERVICE_ACCOUNT = $(error Set TERRAFORM_SERVICE_ACCOUNT in config.mk)

TERRAFORM_STATE_KEY = cloud/gcp/$(GCP_PROJECT_NAME)/blob/$(BLOB_STORAGE_BASE_DOMAIN)

TF_VAR_base_domain = $(BLOB_STORAGE_BASE_DOMAIN)

TF_VAR_project = $(GCP_PROJECT_NAME)

mk.configure += python-minimal terraform terraform-gcp-blob gitlab
mk.configure += terraform-gitlab