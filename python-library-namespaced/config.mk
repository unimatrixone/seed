# The name of this Python package, or the parent package if this is a
# namespaced package.
PYTHON_PKG_NAME=$(error Set PYTHON_PKG_NAME in config.mk)

# The subpackage name in a packaging namespace.
PYTHON_SUBPKG_NAME = $(error Set PYTHON_SUBPKG_NAME in config.mk)

# Choose from 'application' or 'package'.
PROJECT_KIND=$(error Set PROJECT_KIND in config.mk)

# Choose from 'parent' or 'namespaced'. If you are not sure, choose 'parent'.
# If PROJECT_SCOPE=namespaced, then PYTHON_SUBPKG_NAME must also be set.
PROJECT_SCOPE=namespaced

# The Python version to use.
PYTHON_VERSION = 3.9

# Configures the default OS platform. When overriding these variables,
# make sure to use the ?= operator as this value may also be provided as
# an environment variable during a CI run.
#OS_RELEASE_ID ?= alpine
#OS_RELEASE_VERSION ?= 3.12

# Components to configure.
mk.configure += python python-docs python-gitlab docker python-package
mk.configure +=

# User-defined environment variables
