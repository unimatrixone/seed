#!/bin/sh
set -e
CURL="curl --fail --silent -L"
MAKE="make"
MODULE="python-library-namespaced"
SEED_URL="https://gitlab.com/unimatrixone/seed/-/raw/master"
SEED_CONFIG="${SEED_URL}/${MODULE}/config.mk"
if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "Detected MacOS"
else
  echo "Operating system not supported: ${OSTYPE}"
  exit 1
fi

if ! command -v python3 >/dev/null; then
  echo "The python3 command was not found."
  echo "Install Python 3.7 or later from www.python.org/downloads/"
  exit 1
fi
if ! $(python3 -m pip > /dev/null 2>&1); then
  echo "The pip module is not installed. See https://pip.pypa.io/en/stable/installing/"
  exit 1
fi
if ! command -v openssl >/dev/null; then
  echo "The openssl command was not found."
  exit 1
fi
if ! command -v make >/dev/null; then
  echo "The make command was not found."
  exit 1
fi
if ! command -v curl >/dev/null; then
  echo "The curl command was not found."
  exit 1
fi
if ! command -v git >/dev/null; then
  echo "The git command was not found."
  exit 1
fi
if [ -z ${PYTHON_VERSION+x} ]; then
  PYTHON_VERSION=$(python3 -c "import sys; print('%s.%s' % (sys.version_info[0], sys.version_info[1]))")
fi
echo "Using Python ${PYTHON_VERSION}"

git init
touch Makefile\
  && git add Makefile\
  && git commit -m "Initial commit"\
  && git branch -M master mainline
${CURL} ${SEED_URL}/Makefile -o Makefile
${CURL} ${SEED_CONFIG} -o config.mk
if [ ! -z ${PROJECT_NAME+x} ]; then
  sed -i '' "s|PYTHON_PKG_NAME.*|PYTHON_PKG_NAME=${PROJECT_NAME}|g" config.mk
fi
sed -i '' "s|PYTHON_VERSION.*|PYTHON_VERSION=${PYTHON_VERSION}|g" config.mk
mkdir -p ./ops && make configure && make bootstrap && make env
