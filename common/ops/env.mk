# Use this Makefile to configure the operating system specific environments
# for specific contexts. Allowed values are:
#
# gitlab.<os release id>.packages: packages need for the specified operating system
#   during a Gitlab job.
# runtime.<os release id>.packages: packages required at runtime by the application or library.
# build.<os release id>.packages: packages required during build/compile time.
#
# If the list of packages specified in this file gets too long, they should be installed
# priorly in the Docker image or virtual machine environment.
#
runtime.alpine.packages = libressl
runtime.debian.packages = openssl
gitlab.alpine.packages += curl gcc git libc-dev libffi-dev libressl-dev make g++
gitlab.debian.packages += curl gcc git libc-dev libffi-dev libssl-dev make g++