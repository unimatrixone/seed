###############################################################################
#
#		PYTHON-FASTAPI TARGETS
#
###############################################################################
FASTAPI_SEED_URL = $(SEED_URL)/python-fastapi
HTTP_ASGI_MODULE = $(PYTHON_QUALNAME).app.asgi:application
HTTP_LOCAL_PORT ?= 8000
OAUTH2_ACTOR_KEY=pki/pkcs/noop.rsa
PYTHON_INFRA_PACKAGES += gunicorn uvicorn[standard] celery
PYTHON_ASGI_MODULE ?= $(PYTHON_PKG_WORKDIR)/app/asgi.py
PYTHON_RUNTIME_PACKAGES += 'cbra[google]>=2.0.0a24'
UNIMATRIX_SETTINGS_MODULE ?= $(PYTHON_QUALNAME).runtime.settings

# Set some different locations for packages. A package may declare an .asgi
# module which is autodetected by the Unimatrix settings framework.
ifeq ($(PROJECT_KIND), package)
HTTP_ASGI_MODULE = $(PYTHON_QUALNAME).asgi:application
PYTHON_ASGI_MODULE ?= $(PYTHON_PKG_WORKDIR)/asgi.py
endif

app.settings.defaults.seed = $(FASTAPI_SEED_URL)/pkg/runtime/settings/defaults.py
cmd.runhttp ?= $(PYTHON) -m uvicorn $(HTTP_ASGI_MODULE) --reload --reload-dir $(PYTHON_PKG_WORKDIR) --reload-dir $(PYTHON_RUNTIME_LIBS)
cmd.runhttp += --port $(HTTP_LOCAL_PORT)
ifdef LOCALHOST_SSL_KEY
cmd.runhttp += --ssl-keyfile=$(LOCALHOST_SSL_KEY)
endif
ifdef LOCALHOST_SSL_CRT
cmd.runhttp += --ssl-certfile=$(LOCALHOST_SSL_CRT)
endif
docker.build.args += --build-arg RDBMS_MIGRATE='$(RDBMS_MIGRATE)'
docker.build.args += --build-arg HTTP_ASGI_MODULE="$(HTTP_ASGI_MODULE)"
seed.docker.compose ?= $(FASTAPI_SEED_URL)/docker-compose.yml
seed.docker.dockerfile ?= $(FASTAPI_SEED_URL)/Dockerfile
seed.docker.dockerignore ?= $(PYTHON_SEED_URL)/.dockerignore
seed.docker.entrypoint ?= $(SEED_URL)/python-django/bin/docker-entrypoint.sh
seed.gitlab.pipeline ?= $(FASTAPI_SEED_URL)/.gitlab-ci.yml


$(PYTHON_ASGI_MODULE):
	@mkdir -p $(shell dirname $(PYTHON_ASGI_MODULE))
	@$(cmd.curl) $(FASTAPI_SEED_URL)/pkg/app/asgi.py\
		| sed 's|$$PYTHON_PKG_NAME|$(PYTHON_PKG_NAME)|g'\
		> $(PYTHON_ASGI_MODULE)
	@$(cmd.git.add) $(PYTHON_ASGI_MODULE)
	@$(cmd.git) commit -m "Add ASGI application" -- $(PYTHON_ASGI_MODULE)


bootstrap-python-fastapi:
	@find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
	@mkdir -p $(PYTHON_PKG_WORKDIR)/app/tests
	@$(MAKE) $(PYTHON_ASGI_MODULE)
ifeq ($(PROJECT_KIND), application)
	@$(MAKE) $(PYTHON_SETTINGS_PKG)/__init__.py
	@$(cmd.curl) $(FASTAPI_SEED_URL)/pkg/app/tests/test_integration_metadata_endpoints.py\
		> $(PYTHON_PKG_WORKDIR)/app/tests/test_integration_metadata_endpoints.py
	@$(cmd.curl) $(FASTAPI_SEED_URL)/pkg/app/tests/test_system_application_startup.py\
		> $(PYTHON_PKG_WORKDIR)/app/tests/test_system_application_startup.py
	@touch $(PYTHON_PKG_WORKDIR)/app/tests/__init__.py
	@$(cmd.git) add $(PYTHON_PKG_WORKDIR)/app/tests/*
endif


configure-python-fastapi:


screen.conf:
	@$(cmd.curl) $(FASTAPI_SEED_URL)/screen.conf > screen.conf


bootstrap: bootstrap-python-fastapi
runlistener: awaitservices
runhandler: awaitservices
runhttp: migrate
runworker: awaitservices
