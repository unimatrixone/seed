###############################################################################
#
#   TERRAFORM GOOGLE CLOUD PLATFORM NETWORKS
#
#   GNU Make include to create and manage GCP networks using Terraform.
#
###############################################################################
TERRAFORM_TEMPLATE_URL ?= $(SEED_URL)/terraform/templates/cloud/gcp/networks


main.tf: modules/networks/main.tf jinja2
	@$(cmd.curl) $(TERRAFORM_TEMPLATE_URL)/main.tf.j2 | $(cmd.jinja2) > main.tf


modules/networks:
	@mkdir -p modules/networks


modules/networks/main.tf: modules/networks
	@echo "variable \"project\" { type=string }" > ./modules/networks/main.tf


modules/networks/%: modules/networks/main.tf jinja2
	@mkdir -p modules/networks/$(*)
	@$(eval GCP_VPC_NAME=$(*))
	@$(cmd.curl) $(TERRAFORM_TEMPLATE_URL)/modules/networks/main.tf.j2\
		| $(cmd.jinja2) >> modules/networks/main.tf
	@$(cmd.curl) $(TERRAFORM_TEMPLATE_URL)/modules/networks/module.tf.j2\
		| $(cmd.jinja2) > modules/networks/$(*)/main.tf
	@$(TERRAFORM) init


vpc-%:
	@$(MAKE) modules/networks/$(*)


README.md:
	@$(cmd.curl) $(TERRAFORM_TEMPLATE_URL)/README.md > README.md


configure-terraform-gcp-networks: