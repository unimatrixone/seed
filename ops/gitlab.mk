.gitlab-ci.yml:
	@$(cmd.curl) $(or $(SEED_GITLAB_CI), $(error SEED_GITLAB_CI is not defined))\
		> ./.gitlab-ci.yml
	@$(cmd.git.add) ./.gitlab-ci.yml


ops/gitlab:
	@mkdir -p ./ops/gitlab


ops/gitlab/defaults.yml: ops/gitlab
	@$(cmd.curl) $(or $(SEED_GITLAB_DEFAULTS), $(error SEED_GITLAB_DEFAULTS is not defined))\
		> ./ops/gitlab/defaults.yml
	@$(cmd.git.add) ./ops/gitlab/defaults.yml


ops/gitlab/variables.yml: ops/gitlab
	@echo "---\nvariables: {}" > ./ops/gitlab/variables.yml
	@$(cmd.git.add) ./ops/gitlab/variables.yml


configure-gitlab:
gitlab: .gitlab-ci.yml
gitlab: ops/gitlab/defaults.yml
gitlab: ops/gitlab/variables.yml
