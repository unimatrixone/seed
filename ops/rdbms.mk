ifndef DOCKER_COMPOSE
DOCKER_COMPOSE=docker-compose
endif
RDBMS_EXEC=$(DOCKER_COMPOSE) exec rdbms

# Set some default values for the database connection. By default we assume
# a single database configuration. The DB_ENGINE variable is used to determine
# if there was a previous configuration.
DB_HOST ?= 127.0.0.1
DB_NAME ?= rdbms
DB_USERNAME ?= rdbms
DB_PASSWORD ?= rdbms
ifndef DB_ENGINE
DB_ENGINE ?= postgresql
DB_PORT ?= 5432
endif
POSTGRESQL_VERSION ?= 12


cmd.dbclient.postgresql = $(RDBMS_EXEC) psql -U $(DB_USERNAME) -d $(DB_NAME)
cmd.dbexec.postgresql = $(cmd.dbclient.postgresql) -c
cmd.dbdump.postgresql = $(cmd.pg_dump) --clean --if-exists
cmd.pg_dump = $(RDBMS_EXEC) pg_dump -U $(DB_USERNAME) -d $(DB_NAME)
cmd.runsql.postgresql = $(cmd.dbclient.postgresql) -f
ifdef DB_ENGINE


dbdump:
	@$(cmd.dbdump.$(DB_ENGINE))


dbshell:
	@$(cmd.dbclient.$(DB_ENGINE))


dbtruncate-%:
	@$(cmd.dbexec.$(DB_ENGINE)) "TRUNCATE $(*) CASCADE;"


dbtruncate:
ifdef RDBMS_TRUNCATE
	@$(MAKE) $(addprefix dbtruncate-, $(RDBMS_TRUNCATE))
endif


runsql:
ifdef path
	@$(cmd.runsql.$(DB_ENGINE)) $(path)
endif


endif


configure-rdbms:
