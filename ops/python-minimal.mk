###############################################################################
#
#		PYTHON MINIMAL
#
#		Minimal GNU Make include to configure a Python environment.
#
###############################################################################
PYTHON ?= python$(PYTHON_VERSION)
PYTHON_VERSION ?= 3
PYTHON_UNIMATRIX_LIBS = .lib/python/unimatrix
PYTHON_UNIMATRIX_REQUIREMENTS += jinja2-cli

cmd.pip = $(PYTHON) -m pip
cmd.jinja2 = $(PYTHON) ./var/tmp/jinja2


var/tmp/jinja2:
	@mkdir -p ./var/tmp
	@$(cmd.curl) $(SEED_URL)/bin/jinja2 > ./var/tmp/jinja2


$(PYTHON_UNIMATRIX_LIBS):
	@$(cmd.pip) install $(PYTHON_UNIMATRIX_REQUIREMENTS)\
		--target $(PYTHON_UNIMATRIX_LIBS)


configure-python-minimal:
jinja2: var/tmp/jinja2
