###############################################################################
#
#		PYTHON SQLALCHEMY TARGETS
#
###############################################################################
ifeq ($(PROJECT_KIND), application)
ALEMBIC_METADATA=$(PYTHON_QUALNAME).infra.orm:metadata
ALEMBIC_MIGRATIONS_DIR = $(PYTHON_PKG_NAME)/infra/orm/migrations
else
ALEMBIC_METADATA=$(PYTHON_PKG_NAME).orm:metadata
ALEMBIC_MIGRATIONS_DIR = $(PYTHON_SUBPKG_PATH)/orm/migrations
endif
cmd.alembic = $(PYTHON) -m unimatrix.ext.rdbms.migrations --metadata $(ALEMBIC_METADATA)
#cmd.awaitservices = $(PYTHON) -m unimatrix.ext.orm awaitservices
cmd.migrate ?= $(cmd.alembic) upgrade head
cmd.migrations = $(cmd.alembic) revision --autogenerate && $(cmd.git.add) $(ALEMBIC_MIGRATIONS_DIR)
docker.build.args += --build-arg 'RDBMS_MIGRATE=$(cmd.migrate)'
docker.build.args += --build-arg 'ALEMBIC_MIGRATIONS_DIR=$(ALEMBIC_MIGRATIONS_DIR)'
postgresql.python.requirements = psycopg2-binary asyncpg

ALEMBIC_SCRIPT_DIR = alembic
SQLALCHEMY_SEED_URL = $(SEED_URL)/python-sqlalchemy
ifeq ($(PROJECT_SCOPE), namespaced)
SQLALCHEMY_BASE_MODULE = $(PYTHON_QUALNAME).orm
SQLALCHEMY_BASE_MODULE_PATH = $(PYTHON_SUBPKG_PATH)/orm
else
SQLALCHEMY_BASE_MODULE = $(PYTHON_QUALNAME).infra.orm
SQLALCHEMY_BASE_MODULE_PATH := $(PYTHON_PKG_NAME)/infra/orm
endif
PYTHON_SQLALCHEMY_PACKAGES += unimatrix.ext.rdbms
PYTHON_RUNTIME_PACKAGES += $(PYTHON_SQLALCHEMY_PACKAGES) $($(DB_ENGINE).python.requirements)
PYTHON_SQLALCHEMY=1


$(SQLALCHEMY_BASE_MODULE_PATH):
	@mkdir -p $(SQLALCHEMY_BASE_MODULE_PATH)


$(SQLALCHEMY_BASE_MODULE_PATH)/base.py: $(SQLALCHEMY_BASE_MODULE_PATH)
	@$(cmd.curl) $(SQLALCHEMY_SEED_URL)/pkg/infra/orm/base.py\
		> $(SQLALCHEMY_BASE_MODULE_PATH)/base.py
	@$(cmd.git.add) $(SQLALCHEMY_BASE_MODULE_PATH)/base.py\
		&& $(cmd.git) commit -m "Create metadata base module for ORM"


$(SQLALCHEMY_BASE_MODULE_PATH)/__init__.py:
	@$(MAKE) $(SQLALCHEMY_BASE_MODULE_PATH)/base.py
	@$(cmd.curl) $(SQLALCHEMY_SEED_URL)/pkg/infra/orm/__init__.py\
		> $(SQLALCHEMY_BASE_MODULE_PATH)/__init__.py
	@$(cmd.git.add) $(SQLALCHEMY_BASE_MODULE_PATH)/__init__.py\
		&& $(cmd.git) commit -m "Initialize SQLAlchemy ORM package"


$(ALEMBIC_MIGRATIONS_DIR):
	@mkdir -p $(ALEMBIC_MIGRATIONS_DIR)


install-python-sqlalchemy:
	@$(PIP) install\
		$(PYTHON_SQLALCHEMY_PACKAGES)\
		$($(DB_ENGINE).python.requirements)\
		--target .tmp
	@cp -R .tmp/* $(PYTHON_RUNTIME_LIBS)/ && rm -rf .tmp
	@rm -rf $(PYTHON_REQUIREMENTS) && $(MAKE) requirements.txt
	@$(MAKE) alembic.ini
	@$(cmd.git) add -A
	@$(cmd.git) commit -m "Install python-sqlalchemy"


docker-prebuild-python-sqlalchemy:
	@$(MAKE) $(docker.build_dir)/$(ALEMBIC_MIGRATIONS_DIR)


docker-prebuild: docker-prebuild-python-sqlalchemy
migrate: awaitservices
migrations: $(SQLALCHEMY_BASE_MODULE_PATH)/__init__.py
configure-python-sqlalchemy:
