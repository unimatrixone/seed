

.gitlab-ci.yml:
ifeq ($(PROJECT_KIND), application)
	@$(cmd.curl) $(DJANGO_SEED_URL)/.gitlab-ci.project.yml > ./.gitlab-ci.yml
else
	@$(cmd.curl) $(DJANGO_SEED_URL)/.gitlab-ci.app.yml > ./.gitlab-ci.yml
endif
	@$(cmd.git.add) ./.gitlab-ci.yml


ops/gitlab/python-django.mk:
	@$(cmd.curl) $(DJANGO_SEED_URL)/ops/gitlab/python-django.yml > ./ops/gitlab/python-django.yml
	@$(cmd.git.add) ./ops/gitlab/python-django.yml


configure-python-django-gitlab: ops/gitlab/python-django.mk
configure-python-django-gitlab: .gitlab-ci.yml
