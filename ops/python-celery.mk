################################################################################
#
#	CELERY TARGETS
#
################################################################################
CELERY ?= $(PYTHON) -c "from celery.__main__ import main; main()"
CELERY_BROKER_URL = sqla+postgresql://$(DB_USERNAME):$(DB_PASSWORD)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)
CELERY_MODULE ?= $(PYTHON_PKG_NAME).infra.tasks
CELERY_WORKER ?= $(PYTHON_WATCH) -- $(CELERY) -A $(CELERY_MODULE) worker --loglevel DEBUG
PYTHON_RUNTIME_PACKAGES += celery
export

cmd.runworker ?= $(CELERY_WORKER)


configure-python-celery:
