ifndef DOCKER
DOCKER=docker
endif
RDBMS_SVC ?= rdbms
RDBMS_CONTAINER=$(shell $(DOCKER) ps | grep "$$(basename $$PWD)_$(RDBMS_SVC).*" | awk '{ print $$1}')
define POSTGRESQL_SQL_DROPTABLES
DO $$$$
DECLARE tablenames text;
BEGIN
    tablenames := string_agg('"' || tablename || '"', ', ')
        FROM pg_tables WHERE schemaname = 'public';
		IF tablenames IS NOT NULL THEN
      EXECUTE 'DROP TABLE ' || tablenames;
		END IF;
END; $$$$
endef
export
cmd.dbclean.postgresql = $(cmd.dbshell.postgresql)
cmd.dbshell.postgresql = $(DOCKER) exec -it $(RDBMS_CONTAINER) psql -U $(DB_USERNAME)
cmd.dbterminal.postgresql = $(DOCKER) exec -it $(RDBMS_CONTAINER) bash


dbterminal:
	@$(MAKE) dbterminal-$(DB_ENGINE)


dbterminal-postgresql:
	@$(cmd.dbterminal.postgresql)


dbshell:
	@$(MAKE) dbshell-$(DB_ENGINE)


dbshell-postgresql:
	@$(cmd.dbshell.postgresql)


dbclean:
	@$(MAKE) dbclean-$(DB_ENGINE)


dbclean-postgresql:
	@$(cmd.dbshell.postgresql) -c "$${POSTGRESQL_SQL_DROPTABLES}"
