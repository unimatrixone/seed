CLOUDRUN_APP_NAME ?= $(error Define CLOUDRUN_APP_NAME)


docker-publish-%:
	@make docker-image DOCKER_IMAGE_NAME=$(CLOUDRUN_APP_NAME)
	@docker tag $(CLOUDRUN_APP_NAME) $($(*).google.docker-registry)
	@docker push $($(*).google.docker-registry)


deploy-%:
	@make docker-publish-$(*)
	$(foreach location, $($(*).google.cloudrun.locations), make -j4 gcloud-run-deploy DEPLOYMENT_ENV=$(*) region=$(location);)


gcloud-run-deploy:
	@gcloud run deploy $(CLOUDRUN_APP_NAME)\
		--project $($(DEPLOYMENT_ENV).google.project)\
		--image $($(DEPLOYMENT_ENV).google.docker-registry)\
		--region $(region)\
		--args runhttp


configure-google-cloudrun:
