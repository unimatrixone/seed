DAEMONIZE_SERVICES ?= 1
DJANGO_SETTINGS_MODULE=unimatrix.ext.django.settings.runtime
DJANGO_SEED_URL=$(SEED_URL)/python-django
DJANGO_APP_SEED_URL=$(DJANGO_SEED_URL)/app
HTTP_LOCAL_HOST ?= 127.0.0.1
HTTP_LOCAL_PORT ?= 8000
HTTP_WORKER_TIMEOUT ?= 10
HTTP_WSGI_MODULE ?= unimatrix.ext.django.wsgi
RDBMS_MIGRATE=python manage.py migrate
STATIC_ROOT=static
PYTHON_RUNTIME_PACKAGES += django unimatrix.ext.django

# Since the Makefiles are mostly used during development and build, we
# can have some looser standards for a good DX.
HTTP_CSRF_COOKIE_INSECURE ?= 1
HTTP_SESSION_COOKIE_INSECURE ?= 1

# A Python package that is a Django app.
ifeq ($(PROJECT_KIND), package)
DJANGO_URLS_MODULE ?= $(PYTHON_QUALNAME).urls
UNIMATRIX_SETTINGS_MODULE ?= $(PYTHON_QUALNAME).settings
endif

# Packages used for integration with core infrastructure services.
PYTHON_INFRA_PACKAGES += gunicorn uvicorn[standard] celery

# If we are using docker-compose to run the infrastructure, default to
# PostgreSQL.
ifneq ($(wildcard docker-compose.yml),)
DB_ENGINE ?= postgresql
DB_HOST ?= 127.0.0.1
DB_PORT ?= 5432
DB_NAME ?= rdbms
DB_USERNAME ?= rdbms
DB_PASSWORD ?= rdbms
POSTGRESQL_VERSION ?= 12
RABBITMQ_USERNAME ?= rabbitmq
RABBITMQ_PASSWORD ?= rabbitmq
RABBITMQ_PORT ?= 5672
RABBITMQ_VHOST ?= /

export DB_HOST
export DB_PORT
export DB_USERNAME
export DB_PASSWORD
else
DB_ENGINE ?= sqlite
DB_NAME ?= db.sqlite3
endif
export DB_ENGINE
export DB_NAME

# Ensure that the pytest-django plugin is installed.
PYTHON_TEST_PACKAGES += pytest-django

# Command-definitions and parameters
app.settings.defaults.seed = $(DJANGO_SEED_URL)/project_name/settings/defaults.py.tpl
cmd.awaitservices = $(PYTHON) manage.py awaitservices
ifneq ($(wildcard docker-compose.yml),)
cmd.killinfra = docker-compose down
endif
cmd.migrate = $(PYTHON) manage.py migrate
cmd.migrations = $(PYTHON) manage.py makemigrations $(MODULE_NAME)
cmd.runinfra = docker-compose up $(DOCKER_COMPOSE_ARGS)
cmd.runhttp = $(PYTHON) -m gunicorn.app.wsgiapp
cmd.runhttp += $(or $(HTTP_ASGI_MODULE), $(HTTP_WSGI_MODULE))
ifneq ($(HTTP_ASGI_MODULE),)
cmd.runhttp += -k uvicorn.workers.UvicornWorker

# TODO: --reload is not supported by uvicorn.workers.UvicornWorker
# see https://github.com/benoitc/gunicorn/issues/2339
cmd.runhttp += -t $(HTTP_WORKER_TIMEOUT)
endif
cmd.runhttp += --keyfile $(APP_PKIDIR_SERVER)/tls.key
cmd.runhttp += --certfile $(APP_PKIDIR_SERVER)/tls.crt
cmd.runhttp += -b 127.0.0.1:$(HTTP_LOCAL_PORT)
cmd.runhttp += -w 2
cmd.runhttp += --reload
docker.build.args += --build-arg RDBMS_MIGRATE='$(RDBMS_MIGRATE)'
docker.build.args += --build-arg HTTP_WSGI_MODULE="$(HTTP_WSGI_MODULE)"
docker.build.args += --build-arg UNIMATRIX_SETTINGS_MODULE="$(UNIMATRIX_SETTINGS_MODULE)"
docker.build.args += --build-arg RUN_CMD="python manage.py"
pip.package.postgresql = psycopg2-binary
seed.docker.dockerfile ?= $(DJANGO_SEED_URL)/Dockerfile
seed.docker.entrypoint ?= $(DJANGO_SEED_URL)/bin/docker-entrypoint.sh
seed.gitlab.pipeline ?= $(DJANGO_SEED_URL)/.gitlab-ci.yml
seed.python.package ?= $(DJANGO_SEED_URL)/package.json

export POSTGRESQL_VERSION
export RABBITMQ_USERNAME
export RABBITMQ_PASSWORD
export RABBITMQ_VHOST
export RABBITMQ_PORT
export STATIC_ROOT


manage.py:
	@echo "Configuring manage.py with $(UNIMATRIX_SETTINGS_MODULE)"
	@$(cmd.curl) $(DJANGO_SEED_URL)/manage.py.tpl\
		| sed 's/$$UNIMATRIX_SETTINGS_MODULE/$(UNIMATRIX_SETTINGS_MODULE)/g'\
		> manage.py
	@chmod +x manage.py
	@$(cmd.git.add) manage.py
ifeq ($(wildcard requirements.txt),)
	@touch requirements.txt
	@grep -qxF -E '^(django$$|django[\=\<\>]+.*$$)' requirements.txt\
		|| echo 'django' >> requirements.txt
	@grep -qxF -E '^unimatrix.ext.(django$$|django[\=\<\>]+.*$$)' requirements.txt\
		|| echo 'unimatrix.ext.django' >> requirements.txt
	@$(cmd.git.add) requirements.txt
endif


collectstatic:
	@$(PYTHON) manage.py collectstatic --noinput


docker-compose.yml:
	@$(cmd.curl) $(DJANGO_SEED_URL)/docker-compose.yml > docker-compose.yml


screen.conf:
	@$(cmd.curl) $(DJANGO_SEED_URL)/screen.conf > screen.conf


ifeq ($(PROJECT_KIND), application)
DJANGO_URLS_MODULE=$(PYTHON_PKG_NAME).runtime.urls
UNIMATRIX_SETTINGS_MODULE=$(PYTHON_PKG_NAME).runtime.settings


#$(PYTHON_PKG_NAME)/runtime/settings/__init__.py: $(PYTHON_PKG_NAME)/runtime/settings
#	@$(cmd.curl) $(DJANGO_SEED_URL)/project_name/settings/__init__.py.tpl\
#		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_PKG_NAME)/g'\
#		> $(PYTHON_PKG_NAME)/runtime/settings/__init__.py
#	@$(cmd.git.add) $(PYTHON_PKG_NAME)/runtime/settings/__init__.py


#$(PYTHON_PKG_NAME)/runtime/settings/defaults.py: $(PYTHON_PKG_NAME)/runtime/settings/__init__.py
#	@$(cmd.curl) $(DJANGO_SEED_URL)/project_name/settings/defaults.py.tpl\
#		| sed 's/$$DJANGO_URLS_MODULE/$(DJANGO_URLS_MODULE)/g'\
#		| sed 's/$$HTTP_WSGI_MODULE/$(HTTP_WSGI_MODULE)/g'\
#		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_PKG_NAME)/g'\
#		> $(PYTHON_PKG_NAME)/runtime/settings/defaults.py
#	@$(cmd.git.add) $(PYTHON_PKG_NAME)/runtime/settings/defaults.py
#

$(PYTHON_PKG_NAME)/runtime/urls: $(PYTHON_PKG_NAME)/runtime
	@mkdir -p $(PYTHON_PKG_NAME)/runtime/urls


$(PYTHON_PKG_NAME)/runtime/urls/__init__.py: $(PYTHON_PKG_NAME)/runtime/urls
	@echo "from .defaults import *" > $(PYTHON_PKG_NAME)/runtime/urls/__init__.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/runtime/urls/__init__.py


$(PYTHON_PKG_NAME)/runtime/urls/defaults.py: $(PYTHON_PKG_NAME)/runtime/urls/__init__.py
	@$(cmd.curl) $(DJANGO_SEED_URL)/project_name/urls/defaults.py.tpl\
		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_PKG_NAME)/g'\
		> $(PYTHON_PKG_NAME)/runtime/urls/defaults.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/runtime/urls/defaults.py


# For non-namespaced Django application packages, the Django apps are kept in
# */ext.
$(PYTHON_PKG_NAME)/ext/%/admin:
	@mkdir -p $(PYTHON_PKG_NAME)/ext/$(*)/admin
	@$(cmd.curl) $(DJANGO_APP_SEED_URL)/admin/__init__.py\
		> $(PYTHON_PKG_NAME)/ext/$(*)/admin/__init__.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/ext/$(*)/admin/__init__.py


$(PYTHON_PKG_NAME)/ext/%/models:
	@mkdir -p $(PYTHON_PKG_NAME)/ext/$(*)/models
	@echo '# pylint: skip-file' > $(PYTHON_PKG_NAME)/ext/$(*)/models/__init__.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/ext/$(*)/models/__init__.py


$(PYTHON_PKG_NAME)/ext/%/urls.py:
	@mkdir -p $(PYTHON_PKG_NAME)/ext/$(*)
	@$(cmd.curl) $(DJANGO_APP_SEED_URL)/urls.py.tpl\
		| sed 's/$$PYTHON_QUALNAME/$(PYTHON_PKG_NAME).ext.$(*)/g'\
		| sed 's/$$DJANGO_APP_NAME/$(*)/g'\
		> $(PYTHON_PKG_NAME)/ext/$(*)/urls.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/ext/$(*)/urls.py


django-app-%:
	@$(MAKE) $(PYTHON_PKG_NAME)/ext/$(*)/apps.py


manage.py: $(PYTHON_PKG_NAME)/runtime/settings/defaults.py
manage.py: $(PYTHON_PKG_NAME)/runtime/urls/defaults.py
endif

ifeq ($(PROJECT_KIND), package)
# For packages e.g. Django apps, the settings file is an example that is
# used also to test locally. It resides inside the package directory. The
# same applies to the URL configuration.
$(PYTHON_SUBPKG_PATH)/settings.py:
	@$(cmd.curl) $(DJANGO_SEED_URL)/project_name/settings/defaults.py.tpl\
		| sed 's/$$DJANGO_URLS_MODULE/$(DJANGO_URLS_MODULE)/g'\
		| sed 's/$$HTTP_WSGI_MODULE/$(HTTP_WSGI_MODULE)/g'\
		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_QUALNAME)/g'\
		> $(PYTHON_SUBPKG_PATH)/settings.py
	@$(cmd.git.add) $(PYTHON_SUBPKG_PATH)/settings.py


$(PYTHON_SUBPKG_PATH)/urls.py:
	@$(cmd.curl) $(DJANGO_SEED_URL)/project_name/urls/defaults.py.tpl\
		| sed 's/$$PYTHON_PKG_NAME/$(PYTHON_PKG_NAME)/g'\
		> $(PYTHON_SUBPKG_PATH)/urls.py
	@$(cmd.git.add) $(PYTHON_SUBPKG_PATH)/urls.py


manage.py: $(PYTHON_SUBPKG_PATH)/settings.py
manage.py: $(PYTHON_SUBPKG_PATH)/urls.py
endif


$(PYTHON_PKG_NAME)/ext/%/apps.py:
	@mkdir -p $(PYTHON_PKG_NAME)/ext/$(*)
	@$(MAKE) $(PYTHON_PKG_NAME)/ext/$(*)/__init__.py
	@$(cmd.curl) $(DJANGO_APP_SEED_URL)/apps.py.tpl\
		| sed 's/$$PYTHON_QUALNAME/$(PYTHON_PKG_NAME).ext.$(*)/g'\
		| sed 's/$$DJANGO_APP_NAME/$(*)/g'\
		> $(PYTHON_PKG_NAME)/ext/$(*)/apps.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/ext/$(*)/apps.py


$(PYTHON_PKG_NAME)/ext/%/__init__.py:
	@mkdir -p $(PYTHON_PKG_NAME)/ext/$(*)
	@$(cmd.curl) $(DJANGO_APP_SEED_URL)/__init__.py.tpl\
		| sed 's/$$PYTHON_QUALNAME/$(PYTHON_PKG_NAME).ext.$(*)/g'\
		| sed 's/$$DJANGO_APP_NAME/$(*)/g'\
		> $(PYTHON_PKG_NAME)/ext/$(*)/__init__.py
	@$(cmd.git.add) $(PYTHON_PKG_NAME)/ext/$(*)/__init__.py


#install-python-django-packages:
#	@$(PIP) install django --target $(PYTHON_RUNTIME_LIBS)
#	@$(PIP) install unimatrix.ext.django --target $(PYTHON_RUNTIME_LIBS)
#
#
#env: install-python-django-packages


awaitservices: $(PYTHON_RUNTIME_LIBS)
bootstrap: manage.py
configure-python-django: docker-compose.yml
env: docker-compose.yml
runhandler: awaitservices
runhttp: awaitservices
runhttp: collectstatic
runhttp: migrate
runhttp: $(APP_PKIDIR_SERVER)/tls.crt
runhttp: $(PYTHON_RUNTIME_LIBS)
runlistener: awaitservices
runworker: awaitservices
runinfra: docker-compose.yml
watch: awaitservices
