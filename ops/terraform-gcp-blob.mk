TERRAFORM_GCS_BLOB_SEED_URL = $(TERRAFORM_SEED_URL)/templates/cloud/gcp/blob


main.tf: jinja2
ifeq ($(wildcard main.tf),)
	@$(cmd.curl) $(TERRAFORM_GCS_BLOB_SEED_URL)/main.tf.j2 | $(cmd.jinja2)\
		> main.tf
endif


modules/gsc:
	@mkdir -p ./modules/gsc


modules/gsc/main.tf: modules/gsc
	@echo "variable \"base_domain\" { type=string }" > ./modules/gsc/main.tf
	@echo "variable \"project\" { type=string }" >> ./modules/gsc/main.tf


modules/gsc/%/main.tf: modules/gsc/main.tf
	@mkdir -p ./modules/gsc/$(*)
	@$(cmd.curl) $(TERRAFORM_GCS_BLOB_SEED_URL)/gsc.tf.j2 | $(cmd.jinja2)\
		> ./modules/gsc/$(*)/main.tf
	@$(cmd.curl) $(TERRAFORM_GCS_BLOB_SEED_URL)/gsc.module.tf.j2 | $(cmd.jinja2)\
		>> ./modules/gsc/main.tf


gsc-%: jinja2
	@$(MAKE) modules/gsc/$(*)/main.tf BUCKET_NAME=$(*)
	@$(TERRAFROM) init


README.md:
	@$(cmd.curl) $(TERRAFORM_GCS_BLOB_SEED_URL)/README.md > README.md


configure-terraform-gcp-blob: README.md main.tf
