TERRAFORM ?= terraform
TERRAFORM_MODULES_DIR=.terraform
TERRAFORM_PLAN_DST = plan.json
TERRAFORM_SEED_URL ?= $(SEED_URL)/terraform

terraform.init.args ?=
ifdef TERRAFORM_STATE_GOOGLE_SERVICE_ACCOUNT_CREDENTIALS
terraform.init.args += -backend-config "credentials=$(TERRAFORM_STATE_GOOGLE_SERVICE_ACCOUNT_CREDENTIALS)"
endif
ifdef TERRAFORM_PLAN
terraform.apply.args += -auto-approve $(TERRAFORM_PLAN)
endif


apply: $(TERRAFORM_MODULES_DIR)
	@$(TERRAFORM) apply $(terraform.apply.args)


modules:
	@mkdir -p ./modules


plan: $(TERRAFORM_MODULES_DIR)
	@$(TERRAFORM) plan


plan-json:
	@$(TERRAFORM) plan -out=plan.tfplan\
		&& $(TERRAFORM) show -json plan.tfplan > $(TERRAFORM_PLAN_DST)


validate:
	@$(TERRAFORM) validate


.gitignore:
	@$(cmd.curl) $(SEED_URL)/terraform/.gitignore > .gitignore
	@$(cmd.git.add) .gitignore


$(TERRAFORM_MODULES_DIR):
	@$(TERRAFORM) init $(terraform.init.args)


configure-terraform: .gitignore
init: $(TERRAFORM_MODULES_DIR)
