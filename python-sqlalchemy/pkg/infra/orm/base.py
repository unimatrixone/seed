"""Declares the SQLAlchemy declarative base class for all models."""
from unimatrix.conf import settings
from unimatrix.ext.rdbms import declarative_base


Base, metadata = declarative_base(settings=settings)
